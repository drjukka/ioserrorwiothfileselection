//
//  ViewController.swift
//  test
//
//  Created by Jukka Silvennoinen on 24/10/16.
//  Copyright © 2016 DrJukka. All rights reserved.
//

import UIKit

class ViewController: UIViewController{

    @IBOutlet weak var webView: UIWebView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "lokaalisivu", ofType: "html")!)))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

