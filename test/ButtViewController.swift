//
//  ButtViewController.swift
//  test
//
//  Created by Jukka Silvennoinen on 01/12/2016.
//  Copyright © 2016 DrJukka. All rights reserved.
//

import UIKit

class ButtViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttbutt(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.present(next, animated: true, completion: nil)
    }
}
